<?php

namespace CTC\SocialNetworkImportBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use CTC\SocialNetworkImportBundle\Utils\LoggerUtils;

/**
 * Class DeleteSNSubtree
 * @package CTC\SocialNetworkImportBundle\Command
 */
class DeleteSNSubtreeCommand extends ContainerAwareCommand
{


    protected function configure()
    {

        $this
            ->setName('ctc:sn:delete_subtree')
            ->setDescription('Remove Social Network items')
            ->setHelp(<<<'EOF'
                <info>%command.name%</info> Remove Social Network items
EOF
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $defaultParams = $this->getContainer()->getParameter('ctc_social_network_import.params');
        $defaultLocationId = $defaultParams['parentLocationId'];

        $this->output = $output;
        $logger = new LoggerUtils($this->output);
        $logger ->displayLargeMessage("Start delete SN Subtree (Location ID : ".$defaultLocationId.")", 1);

        /** @var $repository \eZ\Publish\API\Repository\Repository */
        $repository = $this->getContainer()->get('ezpublish.api.repository');
        $locationService = $repository->getLocationService();

        $repository->setCurrentUser($repository->getUserService()->loadUser(14));


        try {
            // We first try to load the location so that a NotFoundException is thrown if the Location doesn't exist
            $location = $locationService->loadLocation($defaultLocationId);

            $children = $locationService->loadLocationChildren($location, 0, $locationService->getLocationChildCount($location));

            if(count($children->locations) > 0){
                foreach($children->locations as $child){
                    $locChildren = $locationService->loadLocation($child->id);
                    $locationService->deleteLocation($locChildren);
                    $logger->displayLargeMessage("Delete Location ID ".$child->id." - Name : ".$child->contentInfo->name, 1);
                }
            }
            $logger->displayLargeMessage(count($children->locations) ." Items removed", 1);
        } catch (\eZ\Publish\API\Repository\Exceptions\NotFoundException $e) {
            $output->writeln("No location with id $defaultLocationId");
        }
    }
}


?>