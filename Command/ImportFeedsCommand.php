<?php

namespace CTC\SocialNetworkImportBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;
use CTC\SocialNetworkImportBundle\Utils\LoggerUtils;
use CTC\SocialNetworkImportBundle\Utils\Twitter\TwitterUtils;
use CTC\SocialNetworkImportBundle\Utils\Youtube\YoutubeUtils;
use CTC\SocialNetworkImportBundle\Utils\Instagram\InstagramUtils;
use CTC\SocialNetworkImportBundle\Utils\Facebook\FacebookUtils;
use CTC\SocialNetworkImportBundle\Utils\Facebook\FacebookEventsUtils;

/**
 * Class ImportFeedsCommand
 * @package CTC\SocialNetworkImportBundle\Command
 */
class ImportFeedsCommand extends ContainerAwareCommand
{
    /**
     * @var
     */
    protected $environment;

    /**
     * @var
     */
    protected $input;

    /**
     * @var
     */
    protected $output;

    /**
     * @var
     */
    protected $container;

    /**
     * @var
     */
    protected $repository;

    /**
     * @var
     */
    protected $dryrun;

    /**
     * @var
     */
    protected $social;

    /**
     * @var
     */
    protected $importOnly;

    /**
     * @var
     */
    protected $importParams;

    /**
     * @var
     */
    protected $serviceList;

    /**
     * @var
     */
    protected $servicesParams;

    /**
     * @var \CTC\SocialNetworkImportBundle\Utils\LoggerUtils
     */
    protected $logger;

    /**
     * ImportFeedsCommand constructor.
     * @param $environment
     */
    public function __construct(
        $environment
    ) {
        $this->environment = $environment;
        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('ctc:sn:import')
            ->setDescription('Import social feeds, based on your configuration')
            ->setHelp(<<<'EOF'
<info>%command.name%</info> import social feeds based on your configuration
<comment>dry-run</>, <comment>social</> and <comment>import-only</> are available:
<info>php %command.full_name% --dry-run=true</info> test the app and do not write anything on your server
<info>php %command.full_name% --social=NETWORK_NAME</info> look for a specific social feed 
<info>php %command.full_name% --import-only=true</info> gets feeds but do not create content
EOF
            )
            ->addOption('dry-run', null, InputOption::VALUE_OPTIONAL, 'Try if it works or not', null)
            ->addOption('social', null, InputOption::VALUE_OPTIONAL, 'Specify one specific feed', null)
            ->addOption('import-only', null, InputOption::VALUE_OPTIONAL, 'Specify if we only want to get social network feeds and not create any eZ contents', null);
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->output = $output;
        $this->logger = new LoggerUtils($this->output);
        $this->logger->displayLargeMessage("Start", 1);

        // Checking if we are launching a test
        $dryRun = $input->getOption('dry-run');
        $this->dryrun = false;
        if (isset($dryRun)) {
            $this->logger->displayLargeMessage("Dry run to check if it's ok !", 1, 'fg=white;bg=red');
            $this->dryrun = true;
        }
        // Checking if we import a specific network
        $social = $input->getOption('social');
        $this->social = 'all';
        if (isset($social)) {
            $this->logger->displayLargeMessage("Importing network: " . $social, 1, 'fg=white;bg=green');
            $this->social = $social;
        }
        // Checking if we only want to import
        $importOnly = $input->getOption('import-only');
        $this->importOnly = false;
        if (isset($importOnly)) {
            $this->logger->displayLargeMessage("Importing feeds only", 1, 'fg=white;bg=red');
            $this->importOnly = true;
        }

        // Initialize env
        $this->container = $this->getContainer();
        $this->repository = $this->container->get('ezpublish.api.repository');

        // Loading network configurations
        $this->importParams = $this->container->getParameter("ctc_social_network_import.params");
        $this->serviceList = $this->container->getParameter("ctc_social_network_import.services_enabled");
        $this->servicesParams = $this->container->getParameter("ctc_social_network_import.services_list");

        if(!is_dir($this->importParams['storageDir'])){
            mkdir($this->importParams['storageDir'], 755);
        }
        try {
            // If we only looking for a specific network
            if ($this->social && $this->social != "all") {
                $this->serviceList = array($this->social);
            }
            // Getting feeds
            foreach ($this->serviceList as $service) {
                $this->logger->displayMessage(array("success", "Importing " . $service));
                switch ($service) {
                    case "facebook":
                        $currentParams = $this->servicesParams["facebook"];
                        $facebookUtils = new FacebookUtils($this->repository, $this->container, $this->logger, $this->importParams, $currentParams, $this->dryrun);
                        // Getting feed
                        $facebookUtils->getFacebookFeed();
                        // Importing into BO
                        if (!$this->importOnly) {
                            $facebookUtils->preparePostForContrib();
                        }
                        break;
                    case "facebookevents":
                        $currentParams = $this->servicesParams["facebookevents"];
                        $facebookUtils = new FacebookEventsUtils($this->repository, $this->container, $this->logger, $this->importParams, $currentParams, $this->dryrun);
                        // Getting feed
                        $facebookUtils->getFacebookFeed();
                        // Importing into BO
                        if (!$this->importOnly) {
                            $facebookUtils->preparePostForContrib();
                        }
                        break;
                    case "twitter":
                        $currentParams = $this->servicesParams["twitter"];
                        $twitterUtils = new TwitterUtils($this->repository, $this->container, $this->logger, $this->importParams, $currentParams, $this->dryrun);
                        // Getting feed
                        $twitterUtils->getTwitterFeed();
                        // Importing into BO
                        if (!$this->importOnly) {
                            $twitterUtils->prepareTweetForContrib();
                        }
                        break;
                    case "instagram":
                        $currentParams = $this->servicesParams["instagram"];

                        $eZSNUtils = $this->container->get('ctc.sn.utils.functions');
                        $igUtils = new InstagramUtils($this->repository, $eZSNUtils, $this->logger, $this->importParams, $currentParams, $this->dryrun);
                        // Getting feed
                        $igUtils->getInstagramFeed();
                        // Importing into BO
                        if (!$this->importOnly) {
                            $igUtils->preparePhotoForContrib();
                        }
                        break;
                    case "youtube":
                        $currentParams = $this->servicesParams["youtube"];
                        $youtubeUtils = new YoutubeUtils($this->repository, $this->container, $this->logger, $this->importParams, $currentParams, $this->dryrun);
                        // Getting feed
                        $youtubeUtils->getYoutubeFeed();
                        // Importing into BO
                        if (!$this->importOnly) {
                            $youtubeUtils->prepareVideoForContrib();
                        }
                        break;
                    default:
                        $this->logger->displayMessage(array("error", "Missing configuration: " . $service));
                        break;
                }
            }
        } catch (\Exception $e) {
            $this->logger->displayMessage(array("error", $e->getMessage()));
        }

        $this->logger->displayLargeMessage("End", 1);
    }
}
