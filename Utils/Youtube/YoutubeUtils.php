<?php

namespace CTC\SocialNetworkImportBundle\Utils\Youtube;

use CTC\SocialNetworkImportBundle\Utils\eZSNUtils;
use eZ\Publish\API\Repository\ContentTypeService;
use eZ\Publish\API\Repository\Repository;
use eZ\Publish\API\Repository\Values\Content\LocationQuery;
use function GuzzleHttp\Psr7\build_query;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Filesystem\Filesystem;
use CTC\SocialNetworkImportBundle\Utils\LoggerUtils;
use Symfony\Component\Filesystem\Exception\FileNotFoundException;
use eZ\Publish\API\Repository\Values\Content\Query\Criterion;

/**
 * Class YoutubeUtils
 * @package CTC\SocialNetworkImportBundle\Utils\Youtube
 */
class YoutubeUtils
{
    /**
     * @var \eZ\Publish\API\Repository\Repository
     */
    protected $repository;

    /**
     * @var \Symfony\Component\DependencyInjection\ContainerInterface
     */
    protected $container;

    /**
     * @var \eZ\Publish\API\Repository\LocationService
     */
    protected $locationService;

    /**
     * @var \eZ\Publish\API\Repository\ContentService
     */
    protected $contentService;

    /**
     * @var \eZ\Publish\API\Repository\ContentTypeService
     */
    protected $contentTypeService;

    /**
     * @var \CTC\SocialNetworkImportBundle\Utils\LoggerUtils
     */
    protected $logger;

    /**
     * @var
     */
    protected $globalParams;

    /**
     * @var
     */
    protected $dedicatedParams;

    /**
     * @var string
     */
    protected $ytLocation;

    /**
     * @var string
     */
    protected $fileLocation;

    /**
     * @var string
     */
    protected $createdFileName;

    /**
     * @var string
     */
    protected $imgStorage;

    /**
     * @var Filesystem
     */
    protected $fs;

    /**
     * @var bool
     */
    protected $dryRun;

    /**
     * YoutubeUtils constructor.
     * @param Repository $repository
     * @param ContainerInterface $container
     * @param LoggerUtils $loggerUtils
     * @param $importParams
     * @param $paramsList
     * @param bool $dryRun
     */
    public function __construct(
        Repository $repository,
        ContainerInterface $container,
        LoggerUtils $loggerUtils,
        $importParams,
        $paramsList,
        $dryRun = false)
    {
        // Services
        $this->repository = $repository;
        $this->container = $container;
        $this->locationService = $this->repository->getLocationService();
        $this->contentService = $this->repository->getContentService();
        $this->contentTypeService = $this->repository->getContentTypeService();
        $this->logger = $loggerUtils;
        // Params
        $this->dryRun = $dryRun;
        $this->globalParams = $importParams;
        $this->dedicatedParams = $paramsList;
        $this->ytLocation = $this->globalParams["storageDir"] . '/' . $this->dedicatedParams["serviceFolder"];
        $this->fileLocation = $this->ytLocation . "/" . $this->dedicatedParams["fileName"];
        $this->createdFileName = $this->ytLocation . "/" . $this->dedicatedParams["createdFileName"];
        $this->imgStorage = $this->ytLocation . "/img";
        $this->fs = new Filesystem();
    }

    /**
     * Getting Youtube feed based on configuration
     */
    public function getYoutubeFeed()
    {
        /*
         * TODO: dynamic playlist name fetch
         * Get Channel "uploads" playlist
         * 1- Get channel info : https://content.googleapis.com/youtube/v3/channels?forUsername=CHANNEL_NAME&part=snippet%2CcontentDetails%2Cstatistics&key=API_KEY
         * 2- Find "contentDetails" / "uploads" id
         */
        $this->logger->displayMessage(array("success", "Preparing settings"));

        // Getting needed params in order to fetch videos
        $url = $this->dedicatedParams["appUrl"];
        $params = array(
            "part" => "snippet,contentDetails",
            "maxResults" => $this->dedicatedParams["count"],
            "playlistId" => $this->dedicatedParams["channelUploads"],
            "key" => $this->dedicatedParams["key"]
        );
        try {
            // Getting videos
            if (!$this->fs->exists($this->fileLocation)) {
                $this->logger->displayMessage(array("success", "Calling API"));

                // Fetching api results
                $curlUrl = curl_init($url . 'playlistItems?' . http_build_query($params));
                curl_setopt($curlUrl, CURLOPT_RETURNTRANSFER, TRUE);
                curl_setopt($curlUrl, CURLOPT_POST, FALSE);
                curl_setopt($curlUrl, CURLOPT_HTTPHEADER, array('Content-type: application/json'));

                $response = curl_exec($curlUrl);
                curl_close($curlUrl);

                if (!$this->dryRun) {
                    // Create folders recursively if not exists.
                    $this->fs->mkdir($this->ytLocation, 0775);
                    // Saving video list
                    $this->fs->appendToFile($this->fileLocation, $response);
                }
                $this->logger->displayMessage(array("success", "Videos stored"));
            } else {
                $this->logger->displayMessage(array("success", "File already uploaded"));
            }
        } catch (\Exception $e) {
            $this->logger->displayMessage(array("error", $e->getMessage()));
        }
    }

    /**
     * Read imported feed and contribute items
     *
     * @throws \eZ\Publish\API\Repository\Exceptions\NotFoundException
     */
    public function prepareVideoForContrib()
    {

        $repo = $this->container->get('ezpublish.api.repository');

        $this->logger->displayMessage(array("success", "Initiate eZ Import"));
        // Using admin user
        $this->repository->getPermissionResolver()->setCurrentUserReference($this->repository->getUserService()->loadUser(14));
        try {
            // Checking imported feed
            if ($this->fs->exists($this->fileLocation)) {
                $ytFeed = file_get_contents($this->fileLocation);
                $ytList = json_decode($ytFeed, true);
                // Getting content type
                $contentType = $this->contentTypeService->loadContentTypeByIdentifier($this->globalParams["contentType"]);
                $contentCreateStruct = $this->repository->getContentService()->newContentCreateStruct($contentType, $this->globalParams['language']);
                $locationCreateStruct = $this->locationService->newLocationCreateStruct($this->globalParams['parentLocationId']);


                // Create image storage folders recursively if not exists.
                if (!$this->fs->exists($this->imgStorage)) {
                    if (!$this->dryRun) {
                        $this->fs->mkdir($this->imgStorage, 0775);
                    }
                }
                // Creating content
                $eZSNUtils = $this->container->get('ctc.sn.utils.functions');
                foreach ($ytList["items"] as $video) {

                    $itemName = "Youtube - ".$video["snippet"]['title'];
                    $itemTimestamp = strtotime($video["contentDetails"]["videoPublishedAt"]);

                    // Check if item exists
                    $exist = false;
                    $isExists = $eZSNUtils->checkIfExists($video["contentDetails"]["videoId"]);
                    if($isExists == 1){
                        $this->logger->displayMessage(array("warning", ">>>> Item '".$itemName."' already exists !"));
                        $exist = true;
                    }

                    /*
                     * title: ezstring
                     * content: eztext
                     * image: ezimage
                     * type ezstring => 'youtube'
                     * url_source ezurl
                     * data ezstring
                     * date DateTime
                     */
                    if (!$exist) {
                        $contentCreateStruct->setField("type", "youtube");
                        $contentCreateStruct->setField("data", $video["contentDetails"]["videoId"]);
                        $contentCreateStruct->setField("url_source", "https://www.youtube.com/watch?v=" . $video["contentDetails"]["videoId"]);
                        $contentCreateStruct->setField("date", (int)$itemTimestamp);
                        foreach ($this->dedicatedParams["fields"] as $eZField => $ytField) {
                            if ($eZField == "image") {
                                // Saving thumbnails on disk in order to reduce http calls
                                $tempImgPath = $this->imgStorage . "/" . $video["contentDetails"]["videoId"] . ".jpg";
                                if (!$this->fs->exists($tempImgPath)) {
                                    file_put_contents($tempImgPath, file_get_contents($video["snippet"]["thumbnails"][$ytField]["url"]));
                                }
                                $contentCreateStruct->setField($eZField, new \eZ\Publish\Core\FieldType\Image\Value(array(
                                    'id' => $video["contentDetails"]["videoId"],
                                    'uri' => $tempImgPath,
                                    'alternativeText' => $video["snippet"]["title"],
                                    'inputUri' => $tempImgPath,
                                    'fileName' => $video["contentDetails"]["videoId"] . ".jpg",
                                    'fileSize' => filesize($tempImgPath),
                                    'width' => $video["snippet"]["thumbnails"][$ytField]["width"],
                                    'height' => $video["snippet"]["thumbnails"][$ytField]["height"]
                                )));
                            } elseif ($eZField == "title") {
                                $contentCreateStruct->setField($eZField, $itemName);
                            } elseif (array_key_exists($ytField, $video["snippet"])) {
                                $text = json_encode("");
                                if(strlen(trim($video["snippet"][$ytField])) > 0){
                                    $text = $video["snippet"][$ytField];
                                }
                                $contentCreateStruct->setField($eZField, $text);
                            }
                        }
                        // Publishing content
                        if (!$this->dryRun) {

                            $draft = $this->contentService->createContent($contentCreateStruct, array($locationCreateStruct));
                            $content = $this->contentService->publishVersion($draft->versionInfo);

                            // Retrieve content info and update publishedDate and modificationDate in terms of imported object
                            $contentInfo = $this->contentService->loadContentInfo( $content->id );
                            $contentUpdateStruct = $this->contentService->newContentMetadataUpdateStruct();
                            $contentUpdateStruct->publishedDate = new \DateTime($video["snippet"]["publishedAt"]);
                            $contentUpdateStruct->modificationDate = new \DateTime($video["snippet"]["publishedAt"]);
                            $this->contentService->updateContentMetadata( $contentInfo, $contentUpdateStruct );
                        }

                        $this->logger->displayMessage(array("success", "Content #" . $content->id . ": '" . $content->getName() . "' > ".$video["contentDetails"]["videoId"]." created!"));
                    }
                }

            } else {
                $e = new FileNotFoundException("Error: feed not found", 500, null, $this->fileLocation);
                throw $e;
            }
        } catch (\Exception $exception) {
            $this->logger->displayMessage(array("error", $exception->getMessage()));
        }
    }
}
