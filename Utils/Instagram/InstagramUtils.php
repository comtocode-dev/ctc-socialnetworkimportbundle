<?php

namespace CTC\SocialNetworkImportBundle\Utils\Instagram;

use CTC\SocialNetworkImportBundle\Utils\eZSNUtils;
use eZ\Publish\API\Repository\Repository;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Filesystem\Filesystem;
use CTC\SocialNetworkImportBundle\Utils\LoggerUtils;
use Symfony\Component\Filesystem\Exception\FileNotFoundException;
use Facebook\Facebook;
use Symfony\Component\HttpClient\CurlHttpClient;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

/**
 * Class InstagramUtils
 * @package CTC\SocialNetworkImportBundle\Utils\Instagram
 */
class InstagramUtils
{
	/**
	 * @var \eZ\Publish\API\Repository\Repository
	 */
	protected $repository;
	/**
	 * @var \Symfony\Component\DependencyInjection\ContainerInterface
	 */
	protected $container;
	/**
	 * @var \eZ\Publish\API\Repository\LocationService
	 */
	protected $locationService;
	/**
	 * @var \eZ\Publish\API\Repository\ContentService
	 */
	protected $contentService;
	/**
	 * @var \eZ\Publish\API\Repository\ContentTypeService
	 */
	protected $contentTypeService;
	/**
	 * @var \CTC\SocialNetworkImportBundle\Utils\LoggerUtils
	 */
	protected $logger;

    /**
     * @var eZSNUtils
     */
	protected $snUtils;
	/**
	 * @var
	 */
	protected $globalParams;
	/**
	 * @var
	 */
	protected $dedicatedParams;
	/**
	 * @var string
	 */
	protected $igLocation;
	/**
	 * @var string
	 */
	protected $fileLocation;

	/**
	 * @var string
	 */
	protected $imgStorage;
	/**
	 * @var Filesystem
	 */
	protected $fs;
	/**
	 * @var bool
	 */
	protected $dryRun;

	/**
	 * InstagramUtils constructor.
	 *
	 * @param Repository $repository
	 * @param ContainerInterface $container
	 * @param LoggerUtils $loggerUtils
	 * @param $importParams
	 * @param $paramsList
	 * @param bool $dryRun
	 */
	public function __construct(
		Repository $repository,
		eZSNUtils $snUtils,
		LoggerUtils $loggerUtils,
		$importParams,
		$paramsList,
		$dryRun = false)
	{
		// Services
		$this->repository = $repository;
		$this->snUtils = $snUtils;
		$this->locationService = $this->repository->getLocationService();
		$this->contentService = $this->repository->getContentService();
		$this->contentTypeService = $this->repository->getContentTypeService();
		$this->logger = $loggerUtils;
		// Params
		$this->dryRun = $dryRun;
		$this->globalParams = $importParams;
		$this->dedicatedParams = $paramsList;
		$this->igLocation = $this->globalParams["storageDir"] . '/' . $this->dedicatedParams["serviceFolder"];
		$this->fileLocation = $this->igLocation . "/" . $this->dedicatedParams["fileName"];
		$this->imgStorage = $this->igLocation . "/img";
		$this->fs = new Filesystem();
	}

	/**
	 * Getting Instagram feed based on configuration
	 */
	public function getInstagramFeed()
	{
		$this->logger->displayMessage(array("success", "Preparing settings"));

        $client = new CurlHttpClient();
        $this->logger->displayMessage(array("success", "Preparing settings"));

        $params = array(
            "access_token" => $this->dedicatedParams["appToken"],
            "fields" => "id,media_type,media_url,thumbnail_url,permalink,shortcode,caption,ig_id,timestamp",
            "limit" => $this->dedicatedParams["count"]
        );
        $url = "https://graph.facebook.com/".$this->dedicatedParams["graphAPIVersion"]."/".$this->dedicatedParams["profilId"]."/media?" . http_build_query($params);;

        try {
            $response = $client->request("GET", $url);
            $this->logger->displayMessage(array("success", "Calling endpoint: ".$url));
            $data = json_decode($response->getContent(), true);
            if (!$this->dryRun && array_key_exists("data", $data) && count($data["data"]) > 0) {
                // Create folders recursively if not exists.
                $this->fs->mkdir($this->igLocation, 0775);
                // Saving feed list
                if($this->fs->exists($this->fileLocation)){
                    unlink($this->fileLocation);
                }
                $this->fs->appendToFile($this->fileLocation, json_encode($data["data"]));
                $this->logger->displayMessage(array("success", "Photos stored"));
                $this->logger->displayMessage(array("success", "Posts stored"));
            }
        } catch (\Exception $e) {
            $this->logger->displayMessage(array("error", $e->getMessage()));
        } catch (TransportExceptionInterface $e) {
            $this->logger->displayMessage(array("error", $e->getMessage()));
        }

	}

	/**
	 * Read imported feed and contribute items
	 * @throws \eZ\Publish\API\Repository\Exceptions\NotFoundException
	 */
	public function preparePhotoForContrib()
	{
		$this->logger->displayMessage(array("success", "Initiate eZ Import"));
		// Using admin user
		$this->repository->getPermissionResolver()->setCurrentUserReference($this->repository->getUserService()->loadUser(14));
		try {
            // Checking imported feed
            if ($this->fs->exists($this->fileLocation)) {
                $igFeed = file_get_contents($this->fileLocation);
                $igList = json_decode($igFeed);
                // Getting content type
                $contentType = $this->contentTypeService->loadContentTypeByIdentifier($this->globalParams["contentType"]);
                $contentCreateStruct = $this->repository->getContentService()->newContentCreateStruct($contentType, $this->globalParams['language']);
                $locationCreateStruct = $this->locationService->newLocationCreateStruct($this->globalParams['parentLocationId']);

                // Create image storage folders recursively if not exists.
                if (!$this->fs->exists($this->imgStorage)) {
                    if (!$this->dryRun) {
                        $this->fs->mkdir($this->imgStorage, 0775);
                    }
                }
                // Creating content
                foreach ($igList as $item) {

                    $itemName = "Instagram - " . substr($item->caption, 0, 100);
                    if (strlen($item->caption) > 100) {
                        $itemName .= "...";
                    }
                    // Removing line break and emojis
                    $itemName = trim(preg_replace('/\s+/', ' ', $itemName));
                    $itemName = preg_replace('/[[:^print:]]/', '', $itemName);
                    $itemTimestamp = strtotime($item->timestamp);

                    // Check if item exists
                    $exist = false;
                    $isExists = $this->snUtils->checkIfExists($item->id);
                    if ($isExists == 1) {
                        $this->logger->displayMessage(array("warning", ">>>> Item '" . $itemName . "' already exists !"));
                        $exist = true;
                    }

                    /*
                     * title: ezstring
                     * content: eztext
                     * image: ezimage
                     * type ezstring => 'instagram'
                     * url_source ezurl
                     * data ezstring
                     * date DateTime
                     */
                    if (!$exist) {
                        $igId = $item->id;
                        $contentCreateStruct->setField("type", "instagram");
                        $contentCreateStruct->setField("date", (int)$itemTimestamp);
                        $contentCreateStruct->setField("data", $item->id);
                        $contentCreateStruct->setField("url_source", $item->permalink);
                        foreach ($this->dedicatedParams["fields"] as $eZField => $igField) {
                            if ($eZField == "image") {
                                // Saving thumbnails on disk in order to reduce http calls
                                $tempImgPath = $this->imgStorage . "/" . $igId . ".jpg";
                                if (!$this->fs->exists($tempImgPath)) {
                                    if (isset($item->media_url)) {
                                        $thumbnailSrc = $item->media_url;
                                    }
                                    if ($item->media_type == "VIDEO") {
                                        $thumbnailSrc = $item->thumbnail_url;
                                    }
                                    if (file_get_contents($thumbnailSrc)) {
                                        file_put_contents($tempImgPath, file_get_contents($thumbnailSrc));
                                    }
                                }
                                $imageVal = array(
                                    'id' => $igId,
                                    'uri' => $tempImgPath,
                                    'alternativeText' => $igId,
                                    'inputUri' => $tempImgPath,
                                    'fileName' => $igId . ".jpg",
                                    'fileSize' => filesize($tempImgPath)
                                );
                                $contentCreateStruct->setField($eZField, new \eZ\Publish\Core\FieldType\Image\Value($imageVal));

                            } elseif ($eZField == "title") {
                                $contentCreateStruct->setField("title", $itemName);
                            } elseif (array_key_exists($igField, $item)) {
                                $contentCreateStruct->setField($eZField, json_encode($item->$igField));
                            }
                        }
                        // Publishing content
                        if (!$this->dryRun) {
                            $contentCreateStruct->modificationDate = new \DateTime($item->timestamp);
                            $draft = $this->contentService->createContent($contentCreateStruct, array($locationCreateStruct));
                            $content = $this->contentService->publishVersion($draft->versionInfo);

                            // Retrieve content info and update publishedDate and modificationDate in terms of imported object
                            $contentInfo = $this->contentService->loadContentInfo($content->id);
                            $contentUpdateStruct = $this->contentService->newContentMetadataUpdateStruct();
                            $contentUpdateStruct->publishedDate = new \DateTime($item->timestamp);
                            $contentUpdateStruct->modificationDate = new \DateTime($item->timestamp);
                            $this->contentService->updateContentMetadata($contentInfo, $contentUpdateStruct);

                        }
                        $this->logger->displayMessage(array("success", "Content #" . $content->id . ": '" . $content->getName() . "' created!"));
                    }
                }
                $this->repository->getPermissionResolver()->setCurrentUserReference($this->repository->getUserService()->loadUser(10));
            } else {
                $e = new FileNotFoundException("Error: feed not found", 500, null, $this->fileLocation);
                throw $e;
            }
        }
            // Content type or location not found
        catch ( \eZ\Publish\API\Repository\Exceptions\NotFoundException $e )
        {
            $this->logger->displayMessage(array("error", $exception->getMessage()));
        }
        // Invalid field value
        catch ( \eZ\Publish\API\Repository\Exceptions\ContentFieldValidationException $e )
        {
            $this->logger->displayMessage(array("error", $exception->getMessage()));
        }
        // Required field missing or empty
        catch ( \eZ\Publish\API\Repository\Exceptions\ContentValidationException $e )
        {
            $this->logger->displayMessage(array("error", $exception->getMessage()));

		} catch (\Exception $exception) {
			$this->logger->displayMessage(array("error", $exception->getMessage()));
		}
	}
}
