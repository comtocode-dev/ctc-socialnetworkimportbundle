<?php

namespace CTC\SocialNetworkImportBundle\Utils\Twitter;

use CTC\SocialNetworkImportBundle\Utils\eZSNUtils;
use eZ\Publish\API\Repository\ContentTypeService;
use eZ\Publish\API\Repository\Repository;
use eZ\Publish\API\Repository\Values\Content\LocationQuery;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Filesystem\Filesystem;
use CTC\SocialNetworkImportBundle\Utils\LoggerUtils;
use Symfony\Component\Filesystem\Exception\FileNotFoundException;

use eZ\Publish\API\Repository\Values\Content\Query\Criterion;

/**
 * Class TwitterUtils
 * @package CTC\SocialNetworkImportBundle\Utils\Twitter
 */
class TwitterUtils
{
	/**
	 * @var \eZ\Publish\API\Repository\Repository
	 */
	protected $repository;

	/**
	 * @var \Symfony\Component\DependencyInjection\ContainerInterface
	 */
	protected $container;

	/**
	 * @var \eZ\Publish\API\Repository\LocationService
	 */
	protected $locationService;

	/**
	 * @var \eZ\Publish\API\Repository\ContentService
	 */
	protected $contentService;

	/**
	 * @var \eZ\Publish\API\Repository\ContentTypeService
	 */
	protected $contentTypeService;

	/**
	 * @var \CTC\SocialNetworkImportBundle\Utils\LoggerUtils
	 */
	protected $logger;

	/**
	 * @var
	 */
	protected $globalParams;

	/**
	 * @var
	 */
	protected $dedicatedParams;

	/**
	 * @var string
	 */
	protected $tweetLocation;

	/**
	 * @var string
	 */
	protected $imgStorage;

	/**
	 * @var string
	 */
	protected $fileLocation;

	/**
	 * @var Filesystem
	 */
	protected $fs;

	/**
	 * @var bool
	 */
	protected $dryRun;

	/**
	 * TwitterUtils constructor.
	 * @param Repository $repository
	 * @param ContainerInterface $container
	 * @param LoggerUtils $loggerUtils
	 * @param $importParams
	 * @param $paramsList
	 * @param bool $dryRun
	 */
	public function __construct(
		Repository $repository,
		ContainerInterface $container,
		LoggerUtils $loggerUtils,
		$importParams,
		$paramsList,
		$dryRun = false)
	{
		// Services
		$this->repository = $repository;
		$this->container = $container;
		$this->locationService = $this->repository->getLocationService();
		$this->contentService = $this->repository->getContentService();
		$this->contentTypeService = $this->repository->getContentTypeService();
		$this->logger = $loggerUtils;
		// Params
		$this->dryRun = $dryRun;
		$this->globalParams = $importParams;
		$this->dedicatedParams = $paramsList;
		$this->tweetLocation = $this->globalParams["storageDir"] . '/' . $this->dedicatedParams["serviceFolder"];
		$this->fileLocation = $this->tweetLocation . "/" . $this->dedicatedParams["fileName"];
		$this->imgStorage = $this->tweetLocation . "/img";
		$this->fs = new Filesystem();
	}

	/**
	 * Getting Tweets feed based on configuration
	 */
	public function getTwitterFeed()
	{
		$this->logger->displayMessage(array("success", "Preparing settings"));

		// Cleaning file location
		if ($this->fs->exists($this->fileLocation)) {
			unlink($this->fileLocation);
		}
		// Getting needed params in order to fetch tweets
		$settings = array(
			'oauth_access_token' => $this->dedicatedParams["key"],
			'oauth_access_token_secret' => $this->dedicatedParams["secret"],
			'consumer_key' => $this->dedicatedParams["consumerKey"],
			'consumer_secret' => $this->dedicatedParams["consumerSecret"]
		);
		$url = $this->dedicatedParams["appUrl"];
		$getfield = '?screen_name=' . $this->dedicatedParams["profilName"] . '&count=' . $this->dedicatedParams["count"];
		$requestMethod = 'GET';
		try {
			// Getting tweets
			$this->logger->displayMessage(array("success", "Calling API"));
			/** @var \CTC\SocialNetworkImportBundle\Utils\Twitter\TwitterAPIExchangeUtils $twitter */
			$twitter = new TwitterAPIExchangeUtils($settings);
			$response = $twitter->setGetfield($getfield)->buildOauth($url, $requestMethod)->performRequest();
			if (!$this->dryRun) {
				// Create folders recursively if not exists.
				$this->fs->mkdir($this->tweetLocation, 0775);
				// Saving tweets
				$this->fs->appendToFile($this->fileLocation, $response);
			}
			$this->logger->displayMessage(array("success", "Tweets stored"));

		} catch (\Exception $e) {
			$this->logger->displayMessage(array("error", $e->getMessage()));
		}
	}

	/**
	 * Read imported feed and contribute items
	 *
	 * @throws \eZ\Publish\API\Repository\Exceptions\NotFoundException
	 */
	public function prepareTweetForContrib()
	{
		$repo = $this->container->get('ezpublish.api.repository');

		$this->logger->displayMessage(array("success", "Initiate eZ Import"));
		// Using admin user
		$this->repository->getPermissionResolver()->setCurrentUserReference($this->repository->getUserService()->loadUser(14));
		try {
			// Checking imported feed
			if ($this->fs->exists($this->fileLocation)) {
				$tweetFeed = file_get_contents($this->fileLocation);
				$tweetsList = json_decode($tweetFeed, true);
				// Getting content type
				$contentType = $this->contentTypeService->loadContentTypeByIdentifier($this->globalParams["contentType"]);

				if (!$this->fs->exists($this->imgStorage)) {
					if (!$this->dryRun) {
						$this->fs->mkdir($this->imgStorage, 0775);
					}
				}
				$includeReply = $this->dedicatedParams['includeReply'];
				$includeRetweet = $this->dedicatedParams['includeRetweet'];
				// Creating content
				$eZSNUtils = $this->container->get('ctc.sn.utils.functions');
				foreach ($tweetsList as $tweet) {

					$import = true;
					if(!$includeReply){
						if(isset($tweet['in_reply_to_status_id'])){
							if(!is_null($tweet['in_reply_to_status_id'])){
								$import = false;
							}
						}
					}
					if(!$includeRetweet){
						if(array_key_exists('retweeted_status', $tweet)){
							$import = false;
						}
					}

					$itemName = "Twitter - ".substr($tweet["text"], 0, 100);
					if(strlen($tweet["text"]) > 100){
						$itemName .= "...";
					}
                    // Removing line break and emojis
                    $itemName = trim(preg_replace('/\s+/', ' ', $itemName));
                    $itemName = preg_replace('/[[:^print:]]/', '', $itemName);
					// Default language : eng-GB
					$itemLanguage = "eng-GB";
					if($tweet["lang"] == "fr"){
						$itemLanguage = "fre-FR";
					}
					if($import){
						$itemTimestamp = strtotime($tweet["created_at"]);

						// Check if item exists
						$exist = false;
						$isExists = $eZSNUtils->checkIfExists($tweet["id"]);
						if($isExists == 1){
							$this->logger->displayMessage(array("warning", ">>>> Item '".$itemName."' already exists !"));
							$exist = true;
						}
						/*
						 * title: ezstring
						 * content: eztext
						 * image: ezimage
						 * type ezstring => 'twitter'
						 * url_source ezurl
						 * data ezstring
						 * date DateTime
						 */
						if (!$exist) {

							$contentCreateStruct = $this->repository->getContentService()->newContentCreateStruct($contentType, $itemLanguage);
							$locationCreateStruct = $this->locationService->newLocationCreateStruct($this->globalParams['parentLocationId']);


							$contentCreateStruct->setField("type", "twitter");
							$contentCreateStruct->setField("data", (string)$tweet["id"]);
							$contentCreateStruct->setField("date", $itemTimestamp);
							foreach ($this->dedicatedParams["fields"] as $eZField => $tweetField) {
								if ($eZField == "image") {
									// Saving thumbnails on disk in order to reduce http calls
									if(array_key_exists("media", $tweet["entities"])){

										$tempImgPath = $this->imgStorage . "/" . $tweet["id"] . ".jpg";
										if (!$this->fs->exists($tempImgPath)) {
											file_put_contents($tempImgPath, file_get_contents($tweet["entities"]["media"][0]["media_url_https"]));
										}
										$imageVal = array(
											'id' => (string)$tweet["id"],
											'uri' => (string)$tempImgPath,
											'alternativeText' => (string)$tweet["id"],
											'inputUri' => (string)$tempImgPath,
											'fileName' => (string)$tweet["id"] . ".jpg",
											'fileSize' => (int)filesize($tempImgPath),
											'width' => (int)$tweet["entities"]["media"][0]["sizes"][$tweetField]["w"],
											'height' => (int)$tweet["entities"]["media"][0]["sizes"][$tweetField]["h"]
										);
										$contentCreateStruct->setField($eZField, new \eZ\Publish\Core\FieldType\Image\Value($imageVal));
									}
								} elseif ($eZField == "url_source") {
									$contentCreateStruct->setField($eZField, "https://twitter.com/GroupeSeb/status/" . $tweet[$tweetField]);
								} elseif ($eZField == "title") {
									$contentCreateStruct->setField($eZField, (string)$itemName);
								} elseif (array_key_exists($tweetField, $tweet)) {

									$text = json_encode("");
									if(strlen(trim($tweet[$tweetField])) > 0){
										$text =  json_encode( $tweet[$tweetField] );
									}

									$contentCreateStruct->setField($eZField, $text);
								}

							}
							// Publishing content
							if (!$this->dryRun) {
								$draft = $this->contentService->createContent($contentCreateStruct, array($locationCreateStruct));
								$content = $this->contentService->publishVersion($draft->versionInfo);

								// Retrieve content info and update publishedDate and modificationDate in terms of imported object
								$contentInfo = $this->contentService->loadContentInfo( $content->id );
								$contentUpdateStruct = $this->contentService->newContentMetadataUpdateStruct();
								$contentUpdateStruct->publishedDate = new \DateTime($tweet["created_at"]);
								$contentUpdateStruct->modificationDate = new \DateTime($tweet["created_at"]);
								$this->contentService->updateContentMetadata( $contentInfo, $contentUpdateStruct );
							}


							$this->logger->displayMessage(array("success", "Content #" . $content->id . ": '" . $content->getName() . " [".$itemLanguage."]' created!"));
						}
					}else{
						$this->logger->displayMessage(array("warning", "Content " . $itemName . " [".$itemLanguage."]' not imported because it is a reply or a retweet !"));
					}
				}

				$this->repository->getPermissionResolver()->setCurrentUserReference($this->repository->getUserService()->loadUser(10));
			} else {
				$e = new FileNotFoundException("Error: feed not found", 500, null, $this->fileLocation);
				throw $e;
			}
		} catch (\Exception $exception) {
			$this->logger->displayMessage(array("error", $exception->getMessage()));
		}
	}
}
