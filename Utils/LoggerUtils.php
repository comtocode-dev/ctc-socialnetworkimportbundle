<?php

namespace CTC\SocialNetworkImportBundle\Utils;

use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class LoggerUtils
 * @package CTC\SocialNetworkImportBundle\Utils
 * TODO: handle write into file
 */
class LoggerUtils
{
    /**
     * @var
     */
    protected $output;

    public function __construct(OutputInterface $output)
    {
        $this->output = $output;
    }

    /**
     * Display large message at the begin and at the end of scrip launching
     *
     * @param $message
     * @param int $nbdashline
     * @param string $style
     */
    public function displayLargeMessage($message, $nbdashline = 0, $style = 'fg=black;bg=cyan')
    {
        if ($nbdashline > 0) {
            $dashNb = strlen($message) + 4;
            $dashStr = "";
            for ($i = 0; $i < $dashNb; $i++) {
                $dashStr .= "-";
            }
            for ($j = 0; $j < $nbdashline; $j++) {
                $this->output->writeln("<{$style}>{$dashStr}</>");
            }
        }
        $this->output->writeln("<{$style}>! {$message} !</>");
        if ($nbdashline > 0) {
            for ($j = 0; $j < $nbdashline; $j++) {
                $this->output->writeln("<{$style}>{$dashStr}</>");
            }
        }
    }

    /**
     * Display simple message
     *
     * @param $array
     */
    public function displayMessage($array)
    {
        $message = "<fg=black;bg=green>";
        if ($array[0] == "error") {
            $message = "<fg=black;bg=red>";
        }
        if ($array[0] == "warning") {
            $message = "<fg=black;bg=yellow>";
        }

        $message .= $array[1] . "</>";
        $this->output->writeln($message);
    }
}
