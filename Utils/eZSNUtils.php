<?php

namespace CTC\SocialNetworkImportBundle\Utils;

use eZ\Publish\API\Repository\ContentTypeService;
use eZ\Publish\API\Repository\Repository;
use eZ\Publish\API\Repository\Values\Content\LocationQuery;
use eZ\Publish\API\Repository\Values\Content\Query\Criterion;

/**
 * Class eZSNUtils
 * @package CTC\SocialNetworkImportBundle\Utils
 */
class eZSNUtils
{

    /**
     * @var
     */
    protected $repo;

    protected $cont;

    /**
     * @var \CTC\SocialNetworkImportBundle\Utils\LoggerUtils
     */
    protected $logger;

    public function __construct(Repository $repo, $container)
    {
        $this->repo = $repo;
        $this->cont = $container;
    }


    public function checkIfExists($itemData){

        $defaultParams = $this->cont->getParameter('ctc_social_network_import.params');
        $defaultLocationId = $defaultParams['parentLocationId'];
        $defaultContentType = $defaultParams['contentType'];

        $query = new LocationQuery();
        $query->filter = new Criterion\LogicalAnd(
            array(
                new Criterion\ParentLocationId($defaultLocationId),
                new Criterion\Visibility(Criterion\Visibility::VISIBLE),
                new Criterion\ContentTypeIdentifier(array($defaultContentType)),
                new Criterion\Location\Depth(Criterion\Operator::GTE, 1),
                new Criterion\Field('data', Criterion\Operator::EQ, $itemData)
            )
        );
        $query->limit = 1;
        $res = $this->repo->getSearchService()->findLocations($query)->searchHits;

        if(count($res) > 0){
            $exist = 1;
        }else{
            $exist = 0;
        }
        return $exist;

    }

}