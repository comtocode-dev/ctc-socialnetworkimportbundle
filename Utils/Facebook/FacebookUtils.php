<?php

namespace CTC\SocialNetworkImportBundle\Utils\Facebook;

use eZ\Publish\API\Repository\Repository;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Filesystem\Filesystem;
use CTC\SocialNetworkImportBundle\Utils\LoggerUtils;
use Symfony\Component\Filesystem\Exception\FileNotFoundException;
use Facebook\Facebook;
use Symfony\Component\HttpClient\CurlHttpClient;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

/**
 * Class FacebookUtils
 * @package CTC\SocialNetworkImportBundle\Utils\Facebook
 */
class FacebookUtils
{
    /**
     * @var \eZ\Publish\API\Repository\Repository
     */
    protected $repository;
    /**
     * @var \Symfony\Component\DependencyInjection\ContainerInterface
     */
    protected $container;
    /**
     * @var \eZ\Publish\API\Repository\LocationService
     */
    protected $locationService;
    /**
     * @var \eZ\Publish\API\Repository\ContentService
     */
    protected $contentService;
    /**
     * @var \eZ\Publish\API\Repository\ContentTypeService
     */
    protected $contentTypeService;
    /**
     * @var \CTC\SocialNetworkImportBundle\Utils\LoggerUtils
     */
    protected $logger;
    /**
     * @var
     */
    protected $globalParams;
    /**
     * @var
     */
    protected $dedicatedParams;
    /**
     * @var string
     */
    protected $fbLocation;
    /**
     * @var string
     */
    protected $fileLocation;
    /**
     * @var string
     */
    protected $createdFileName;
    /**
     * @var string
     */
    protected $imgStorage;
    /**
     * @var Filesystem
     */
    protected $fs;
    /**
     * @var bool
     */
    protected $dryRun;


    /**
     * FacebookUtils constructor.
     *
     * @param Repository $repository
     * @param ContainerInterface $container
     * @param LoggerUtils $loggerUtils
     * @param $importParams
     * @param $paramsList
     * @param bool $dryRun
     */
    public function __construct(
        Repository $repository,
        ContainerInterface $container,
        LoggerUtils $loggerUtils,
        $importParams,
        $paramsList,
        $dryRun = false)
    {
        // Services
        $this->repository = $repository;
        $this->container = $container;
        $this->locationService = $this->repository->getLocationService();
        $this->contentService = $this->repository->getContentService();
        $this->contentTypeService = $this->repository->getContentTypeService();
        $this->logger = $loggerUtils;
        // Params
        $this->dryRun = $dryRun;
        $this->globalParams = $importParams;
        $this->dedicatedParams = $paramsList;
        $this->fbLocation = $this->globalParams["storageDir"] . '/' . $this->dedicatedParams["serviceFolder"];
        $this->fileLocation = $this->fbLocation . "/" . $this->dedicatedParams["fileName"];
        $this->createdFileName = $this->fbLocation . "/" . $this->dedicatedParams["createdFileName"];
        $this->imgStorage = $this->fbLocation . "/img";
        $this->fs = new Filesystem();
    }

    /**
     * Getting Facebook feed based on configuration
     */
    public function getFacebookFeed()
    {
        $client = new CurlHttpClient();
        $this->logger->displayMessage(array("success", "Preparing settings"));

        $params = array(
            "access_token" => $this->dedicatedParams["appToken"],
            "fields" => "created_time,message,story,id,full_picture,permalink_url"
        );

        $url = "https://graph.facebook.com/".$this->dedicatedParams["graphAPIVersion"]."/".$this->dedicatedParams["profilName"]."/feed?" . http_build_query($params);;

        try {
            $response = $client->request("GET", $url);
            $this->logger->displayMessage(array("success", "Calling endpoint: ".$url));

            $data = json_decode($response->getContent(), true);

            if (!$this->dryRun && array_key_exists("data", $data) && count($data["data"]) > 0) {
                // Create folders recursively if not exists.
                $this->fs->mkdir($this->fbLocation, 0775);
                // Saving feed list
                if($this->fs->exists($this->fileLocation)){
                    unlink($this->fileLocation);
                }
                $this->fs->appendToFile($this->fileLocation, json_encode($data["data"]));
                $this->logger->displayMessage(array("success", "Posts stored"));
            }
        } catch (\Exception $e) {
            $this->logger->displayMessage(array("error", $e->getMessage()));
        } catch (TransportExceptionInterface $e) {
            $this->logger->displayMessage(array("error", $e->getMessage()));
        }

    }

    /**
     * Read imported feed and contribute items
     * @throws \eZ\Publish\API\Repository\Exceptions\NotFoundException
     */
    public function preparePostForContrib()
    {
        $this->logger->displayMessage(array("success", "Initiate eZ Import"));
        // Using admin user
        $this->repository->getPermissionResolver()->setCurrentUserReference($this->repository->getUserService()->loadUser(14));
        try {
            // Checking imported feed
            if ($this->fs->exists($this->fileLocation)) {
                $feed = file_get_contents($this->fileLocation);
                $list = json_decode($feed, true);

                // Getting content type
                $contentType = $this->contentTypeService->loadContentTypeByIdentifier($this->globalParams["contentType"]);
                $contentCreateStruct = $this->repository->getContentService()->newContentCreateStruct($contentType, $this->globalParams['language']);
                $locationCreateStruct = $this->locationService->newLocationCreateStruct($this->globalParams['parentLocationId']);

                // Create image storage folders recursively if not exists.
                if (!$this->fs->exists($this->imgStorage)) {
                    if (!$this->dryRun) {
                        $this->fs->mkdir($this->imgStorage, 0775);
                    }
                }
                // Creating content
                $eZSNUtils = $this->container->get('ctc.sn.utils.functions');
                foreach ($list as $item) {
                    if(array_key_exists('message', $item) && array_key_exists('full_picture', $item)){
                        $itemName = "Facebook - ".substr(strip_tags($item['message']), 0, 100);
                        if(strlen($item['message']) > 100){
                            $itemName .= "...";
                        }
                        // Removing line break and emojis
                        $itemName = trim(preg_replace('/\s+/', ' ', $itemName));
                        $itemName = preg_replace('/[[:^print:]]/', '', $itemName);
                        $itemDate = $item['created_time'];
                        $itemTimestamp = strtotime($item['created_time']);
                        $itemID = $item['id'];
                        $itemLink = $item['permalink_url'];
                        $itemPicture = $item['full_picture'];

                        // Check if item exists
                        $exist = false;
                        $isExists = $eZSNUtils->checkIfExists($itemID);
                        if($isExists == 1){
                            $this->logger->displayMessage(array("warning", ">>>> Item '".$itemName."' already exists !"));
                            $exist = true;
                        }

                        if (!$exist) {
                            $contentCreateStruct->setField("type", "facebook");
                            $contentCreateStruct->setField("date", (int)$itemTimestamp);
                            $contentCreateStruct->setField("data", $itemID);
                            $contentCreateStruct->setField("url_source", $itemLink);
                            foreach ($this->dedicatedParams["fields"] as $eZField => $field) {
                                if ($eZField == "image") {
                                    // Saving thumbnails on disk in order to reduce http calls
                                    $tempImgPath = $this->imgStorage . "/" . $itemID . ".jpg";
                                    if (!$this->fs->exists($tempImgPath)) {
                                        if(file_get_contents($itemPicture)){
                                            $thumbnailSrc = $itemPicture;
                                            file_put_contents($tempImgPath, file_get_contents($thumbnailSrc));
                                        }
                                    }

                                    $imageVal = array(
                                        'id' => $itemID,
                                        'uri' => $tempImgPath,
                                        'alternativeText' => $itemID,
                                        'inputUri' => $tempImgPath,
                                        'fileName' => $itemID . ".jpg",
                                        'fileSize' => filesize($tempImgPath)
                                    );
                                    $contentCreateStruct->setField($eZField, new \eZ\Publish\Core\FieldType\Image\Value($imageVal));

                                } elseif ($eZField == "title") {
                                    $contentCreateStruct->setField("title", $itemName);
                                } elseif (array_key_exists($field, $item)) {
                                    $contentCreateStruct->setField($eZField, json_encode($item[$field]));
                                }
                            }
                            // Publishing content
                            if (!$this->dryRun) {
                                $contentCreateStruct->modificationDate = new \DateTime($itemDate);
                                $draft = $this->contentService->createContent($contentCreateStruct, array($locationCreateStruct));
                                $content = $this->contentService->publishVersion($draft->versionInfo);
                                // Retrieve content info and update publishedDate and modificationDate in terms of imported object
                                $contentInfo = $this->contentService->loadContentInfo( $content->id );
                                $contentUpdateStruct = $this->contentService->newContentMetadataUpdateStruct();
                                $contentUpdateStruct->publishedDate = new \DateTime($itemDate);
                                $contentUpdateStruct->modificationDate = new \DateTime($itemDate);
                                $this->contentService->updateContentMetadata( $contentInfo, $contentUpdateStruct );

                            }

                            $this->logger->displayMessage(array("success", "Content #" . $content->id . ": '" . $content->getName() . "' created!"));
                        }
                    }
                }
                $this->repository->getPermissionResolver()->setCurrentUserReference($this->repository->getUserService()->loadUser(10));
            } else {
                $e = new FileNotFoundException("Error: feed not found", 500, null, $this->fileLocation);
                throw $e;
            }
        } catch (\Exception $exception) {
            $this->logger->displayMessage(array("error", $exception->getMessage()));
        }
    }
}
