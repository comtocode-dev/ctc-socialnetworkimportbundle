<?php

namespace CTC\SocialNetworkImportBundle\Utils\Facebook;

use eZ\Publish\API\Repository\Repository;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Filesystem\Filesystem;
use CTC\SocialNetworkImportBundle\Utils\LoggerUtils;
use Symfony\Component\Filesystem\Exception\FileNotFoundException;
use Facebook\Facebook;
use Symfony\Component\HttpClient\CurlHttpClient;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

/**
 * Class FacebookEventsUtils
 * @package CTC\SocialNetworkImportBundle\Utils\Facebook
 */
class FacebookEventsUtils
{
    /**
     * @var \eZ\Publish\API\Repository\Repository
     */
    protected $repository;
    /**
     * @var \Symfony\Component\DependencyInjection\ContainerInterface
     */
    protected $container;
    /**
     * @var \eZ\Publish\API\Repository\LocationService
     */
    protected $locationService;
    /**
     * @var \eZ\Publish\API\Repository\ContentService
     */
    protected $contentService;
    /**
     * @var \eZ\Publish\API\Repository\ContentTypeService
     */
    protected $contentTypeService;
    /**
     * @var \CTC\SocialNetworkImportBundle\Utils\LoggerUtils
     */
    protected $logger;
    /**
     * @var
     */
    protected $globalParams;
    /**
     * @var
     */
    protected $dedicatedParams;
    /**
     * @var string
     */
    protected $fbLocation;
    /**
     * @var string
     */
    protected $fileLocation;
    /**
     * @var string
     */
    protected $createdFileName;
    /**
     * @var string
     */
    protected $imgStorage;
    /**
     * @var Filesystem
     */
    protected $fs;
    /**
     * @var bool
     */
    protected $dryRun;

    /**
     * FacebookUtils constructor.
     *
     * @param Repository $repository
     * @param ContainerInterface $container
     * @param LoggerUtils $loggerUtils
     * @param $importParams
     * @param $paramsList
     * @param bool $dryRun
     */
    public function __construct(
        Repository $repository,
        ContainerInterface $container,
        LoggerUtils $loggerUtils,
        $importParams,
        $paramsList,
        $dryRun = false)
    {
        // Services
        $this->repository = $repository;
        $this->container = $container;
        $this->locationService = $this->repository->getLocationService();
        $this->contentService = $this->repository->getContentService();
        $this->contentTypeService = $this->repository->getContentTypeService();
        $this->logger = $loggerUtils;
        // Params
        $this->dryRun = $dryRun;
        $this->globalParams = $importParams;
        $this->dedicatedParams = $paramsList;
        $this->fbLocation = $this->globalParams["storageDir"] . '/' . $this->dedicatedParams["serviceFolder"];
        $this->fileLocation = $this->fbLocation . "/" . $this->dedicatedParams["fileName"];
        $this->createdFileName = $this->fbLocation . "/" . $this->dedicatedParams["createdFileName"];
        $this->imgStorage = $this->fbLocation . "/img";
        $this->fs = new Filesystem();
    }

    /**
     * Getting Facebook feed based on configuration
     * @throws \Facebook\Exceptions\FacebookSDKException
     */
    public function getFacebookFeed()
    {
        $this->logger->displayMessage(array("success", "Preparing settings"));

        $client = new CurlHttpClient();
        $this->logger->displayMessage(array("success", "Preparing settings"));

        $params = array(
            "access_token" => $this->dedicatedParams["appToken"],
            "fields" => "description,start_time,end_time,cover,name,id",
            "time_filter" => "upcoming"
        );

        $url = "https://graph.facebook.com/".$this->dedicatedParams["graphAPIVersion"]."/".$this->dedicatedParams["profilName"]."/events?" . http_build_query($params);;

        try {
            $response = $client->request("GET", $url);
            $this->logger->displayMessage(array("success", "Calling endpoint: ".$url));

            $data = json_decode($response->getContent(), true);

            if (!$this->dryRun && array_key_exists("data", $data) && count($data["data"]) > 0) {
                // Create folders recursively if not exists.
                $this->fs->mkdir($this->fbLocation, 0775);
                // Saving feed list
                if($this->fs->exists($this->fileLocation)){
                    unlink($this->fileLocation);
                }
                $this->fs->appendToFile($this->fileLocation, json_encode($data["data"]));
                $this->logger->displayMessage(array("success", "Posts stored"));
            }
        } catch (\Exception $e) {
            $this->logger->displayMessage(array("error", $e->getMessage()));
        } catch (TransportExceptionInterface $e) {
            $this->logger->displayMessage(array("error", $e->getMessage()));
        }
    }

    /**
     * Read imported feed and contribute items
     * @throws \eZ\Publish\API\Repository\Exceptions\NotFoundException
     */
    public function preparePostForContrib()
    {
        $this->logger->displayMessage(array("success", "Initiate eZ Import"));
        // Using admin user
        $this->repository->getPermissionResolver()->setCurrentUserReference($this->repository->getUserService()->loadUser(14));
        try {
            // Checking imported feed
            if ($this->fs->exists($this->fileLocation)) {
                $fbFeed = file_get_contents($this->fileLocation);
                $fbList = json_decode($fbFeed);
                // Getting content type
                $contentType = $this->contentTypeService->loadContentTypeByIdentifier($this->dedicatedParams["contentType"]);
                $contentCreateStruct = $this->repository->getContentService()->newContentCreateStruct($contentType, $this->globalParams['language']);
                $locationCreateStruct = $this->locationService->newLocationCreateStruct($this->dedicatedParams['parentLocationId']);
                // Checking previously imported items
                if (!$this->fs->exists($this->createdFileName)) {
                    if (!$this->dryRun) {
                        // Create folders recursively if not exists.
                        $this->fs->mkdir($this->fbLocation, 0775);
                        // Creating empty file
                        $this->fs->touch($this->createdFileName);
                    }
                }
                // Getting list
                $importedEvents = json_decode(file_get_contents($this->createdFileName), true);
                // Create image storage folders recursively if not exists.
                if (!$this->fs->exists($this->imgStorage)) {
                    if (!$this->dryRun) {
                        $this->fs->mkdir($this->imgStorage, 0775);
                    }
                }
                // Creating content
                $newlyCreatedItems = array();
                foreach ($fbList as $item) {
                    $exist = false;
                    $itemTimestamp = strtotime($item->start_time);
                    if (!is_null($importedEvents)) {
                        foreach ($importedEvents as $imported) {
                            if ($itemTimestamp == $imported["created_at"]) {
                                $exist = true;
                                break;
                            }
                        }
                    }
                    /*
                     * name: ezstring
                     * image: ezimage
                     * content: eztext
                     * external_link ezurl
                     * event_date DateTime
                     */
                    if (!$exist) {
                        $fbId = $item->id;
                        $contentCreateStruct->setField("event_date", (int)$itemTimestamp);
                        $contentCreateStruct->setField("external_link", "https://www.facebook.com/events/" . $fbId);
                        foreach ($this->dedicatedParams["fields"] as $eZField => $fbField) {
                            if ($eZField == "image") {
                                if (property_exists($item, "cover")) {
                                    // Saving thumbnails on disk in order to reduce http calls
                                    $tempImgPath = $this->imgStorage . "/" . $fbId . ".jpg";
                                    if (!$this->fs->exists($tempImgPath)) {
                                        $thumbnailSrc = $item->cover->source;
                                        file_put_contents($tempImgPath, file_get_contents($thumbnailSrc));
                                    }
                                    $contentCreateStruct->setField($eZField, new \eZ\Publish\Core\FieldType\Image\Value(array(
                                        'id' => $fbId,
                                        'uri' => $tempImgPath,
                                        'alternativeText' => $fbId,
                                        'inputUri' => $tempImgPath,
                                        'fileName' => $fbId . ".jpg",
                                        'fileSize' => filesize($tempImgPath)
                                    )));
                                }
                            } elseif (array_key_exists($fbField, $item)) {
                                $contentCreateStruct->setField($eZField, json_encode($item->$fbField));
                            }
                        }
                        // Publishing content
                        if (!$this->dryRun) {
                            $contentCreateStruct->modificationDate = new \DateTime($item->start_time);
                            $draft = $this->contentService->createContent($contentCreateStruct, array($locationCreateStruct));
                            $content = $this->contentService->publishVersion($draft->versionInfo);
                        }
                        $newlyCreatedItems[] = array(
                            "contentId" => $content->id,
                            "name" => $content->getName(),
                            "created_at" => $itemTimestamp
                        );
                        $this->logger->displayMessage(array("success", "Content #" . $content->id . ": '" . $content->getName() . "' created!"));
                    }
                }
                if (!$this->dryRun) {
                    if (!is_null($importedEvents)) {
                        $newlyCreatedItems = array_merge($importedEvents, $newlyCreatedItems);
                    }
                    $this->fs->dumpFile($this->createdFileName, json_encode($newlyCreatedItems));
                }
                $this->repository->getPermissionResolver()->setCurrentUserReference($this->repository->getUserService()->loadUser(10));
            } else {
                $e = new FileNotFoundException("Error: feed not found", 500, null, $this->fileLocation);
                throw $e;
            }
        } catch (\Exception $exception) {
            $this->logger->displayMessage(array("error", $exception->getMessage()));
        }
    }
}
