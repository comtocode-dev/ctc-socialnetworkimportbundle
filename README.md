Com to Code Social Network Import Bundle
=======================

This bundle generates content based on configured social network feeds.

It allows to remove all social network content if you need it.

## Installation
app/AppKernel.php:
```
    new CTC\SocialNetworkImportBundle\CTCSocialNetworkImportBundle(),
```

## Configuration
the following configuration options are defined in the config.yml file

```yml
ctc_social_network_import:
    # Global params
    params:
        storageDir: '%kernel.project_dir%/var/import'
        limit: 50
        parentLocationId: 2
        contentType: "social_network"
        language: "fre-FR"
    # Currently enabled apis
    services_enabled:
        - "facebook"
        - ...
    # Services params
    services_list:
        facebook:
            serviceFolder: "fb"
            appId: "XXX"
            appSecret: "XXX"
            appToken: "XXX"
            profilName: "XXX"
            graphAPIVersion: "v15.0"
            fileName: "last_posts.json"
            createdFileName: "content_created.json"
            count: 50
            fields:
                title: "id"
                content: "message"
                image: "full_picture"
        ...
```

## Import social content
### Running the command
To run the command from your console:
```bash
php bin/console ctc:sn:import
php bin/console ctc:sn:import --dry-run=true
php bin/console ctc:sn:import --import-only=true

php bin/console ctc:sn:import --social=[service_item]

example : 
php bin/console ctc:sn:import --social=instagram
php bin/console ctc:sn:import --social=facebook
php bin/console ctc:sn:import --social=youtube
php bin/console ctc:sn:import --social=twitter
```
## Remove social content
### Running the command
To run the command from your console:
```bash
php bin/console ctc:sn:delete_subtree
```
This will import feeds and create eZ contents

## Supported service_item
- facebook
- facebook events
- instagram
- twitter
- youtube

## Remove all social content from subtree

### Running the command
To run the command from your console:
```bash
php bin/console ctc:sn:delete_subtree
```

This will import feeds and create eZ contents

# Changelog

## v.1.0.8

### Facebook

- Update Facebook SDK 
- Update Facebook / Facebook Event and Instagram feed import

## v.1.0.5

### Twitter

Fix issue for twitter parameter (`in_reply_to_status_id`)

### Instagram

Fix issue related to instagram thumbnail for videos

## v.1.0.4

### Facebook and Instagram

Upgrade Facebook Api version to 7.

## v.1.0.3

### Twitter

Add improvement to display or not replies and retweet. 

On your configuration flow, you can insert these values : 

```$yml
    twitter:
        serviceFolder: "tw"
        appUrl: "https://api.twitter.com/1.1/statuses/user_timeline.json"
        key: "[YOUR_API_KEY]"
        secret: "[YOUR_SECRET_KEY]"
        consumerKey: "[YOUR_CUSTOMER_KEY]"
        consumerSecret: "[YOUR_CUSTOMER_SECRET]"
        profilName: "[YOUR_TWITTER_ID]"
        fileName: "last_tweets.json"
        createdFileName: "content_created.json"
        includeReply: false|true # If false : Exclude replies (default : true)
        includeRetweet: false|true # If false : Exclude retweet (default : true)
        count: 500
        fields:
            title: "id_str"
            content: "text"
            url_source: "id_str"
``` 
