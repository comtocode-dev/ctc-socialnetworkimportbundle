<?php

namespace CTC\SocialNetworkImportBundle\Twig;

use eZ\Publish\API\Repository\Repository;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;
use eZ\Publish\API\Repository\Values\Content\Query;
use eZ\Publish\API\Repository\Values\Content\LocationQuery;
use eZ\Publish\API\Repository\Values\Content\Query\Criterion;
use eZ\Publish\API\Repository\Values\Content\Query\SortClause;

/**
 * Class SocialNetworkFunctions
 * @package CTC\BaseBundle\Twig
 */
class SocialNetworkFunctions extends \Twig_Extension
{
    /**
     * @var Repository
     */
    protected $repository;
    /**
     * @var \eZ\Publish\API\Repository\LocationService
     */
    protected $locationService;
    /**
     * @var array
     */
    protected $params;
    /**
     * @var null|Container
     */
    private $container;

    /**
     * CTCFunctions constructor.
     *
     * @param Container $container
     * @param Repository $repository
     */
    public function __construct(Container $container, Repository $repository)
    {
        $this->container = $container;
        $this->repository = $repository;
        $this->locationService = $repository->getLocationService();
        $this->params = $container->getParameter("ctc_social_network_import.params");
    }

    /**
     * @param $pageNum
     * @param int $limit
     * @param bool $category
     * @param array $type
     *
     * @return array
     * @throws \eZ\Publish\API\Repository\Exceptions\InvalidArgumentException
     * @throws \eZ\Publish\API\Repository\Exceptions\NotFoundException
     * @throws \eZ\Publish\API\Repository\Exceptions\UnauthorizedException
     */
    public function getLiveFeed($pageNum, $limit = 0, $category = false, $type = array())
    {
        // Creating search request
        $query = new LocationQuery();
        // Getting every social except youtube
        $queryFilter = array(
            new Criterion\Subtree($this->locationService->loadLocation($this->params["parentLocationId"])->pathString),
            new Criterion\Visibility(Criterion\Visibility::VISIBLE),
            new Criterion\ContentTypeIdentifier(array($this->params["contentType"])),
            new Criterion\Location\Depth(Criterion\Operator::GTE, 1),
        );
        // Handle Category filter
        if ($category) {
            $queryFilter[] = new Criterion\Field('category', Criterion\Operator::EQ, $category);
        }
        // Handle Type filter
        if (!empty($type)) {
            $queryFilter[] = new Criterion\Field('type', Criterion\Operator::IN, $type);
        }
        $query->filter = new Criterion\LogicalAnd($queryFilter);
        $query->limit = $limit > 0 ? $limit : $this->params["limit"];
        $query->offset = $pageNum > 1 ? ($query->limit * ($pageNum - 1)) : 0;
        // Adding sort clause(s)
        $query->sortClauses = array(
            new SortClause\Field($this->params["contentType"], 'date', Query::SORT_DESC)
        );
        $elements = $this->repository->getSearchService()->findLocations($query);
        // Getting locations
        $locationArray = array();
        foreach ($elements->searchHits as $key => $element) {
            $locationArray[] = $element->valueObject;
        }
        $locations = array(
            "page" => $pageNum,
            "locations" => $locationArray,
            "doIPaginate" => (($query->limit + $query->offset) < $elements->totalCount ? true : false)
        );

        return $locations;
    }

    /**
     * @param $type
     * @param int $offset
     * @param bool $limit
     *
     * @return bool|\eZ\Publish\API\Repository\Values\Content\Search\SearchHit[]|\eZ\Publish\API\Repository\Values\ValueObject
     * @throws \eZ\Publish\API\Repository\Exceptions\InvalidArgumentException
     * @throws \eZ\Publish\API\Repository\Exceptions\NotFoundException
     * @throws \eZ\Publish\API\Repository\Exceptions\UnauthorizedException
     */
    public function getSocialItem($type, $offset = 0, $limit = true)
    {
        // Creating search request
        $query = new LocationQuery();
        $query->filter = new Criterion\LogicalAnd(
            array(
                new Criterion\Subtree($this->locationService->loadLocation($this->params["parentLocationId"])->pathString),
                new Criterion\Visibility(Criterion\Visibility::VISIBLE),
                new Criterion\ContentTypeIdentifier(array($this->params["contentType"])),
                new Criterion\Location\Depth(Criterion\Operator::GTE, 1),
                new Criterion\Field('type', Criterion\Operator::EQ, $type)
            )
        );
        // Adding sort clause(s)
        $query->sortClauses = array(
            new SortClause\Field($this->params["contentType"], 'date', Query::SORT_DESC)
        );
        $query->offset = $offset;
        $query->limit = $limit ? 1 : PHP_INT_MAX;
        $elements = $this->repository->getSearchService()->findLocations($query);
        if ($elements->totalCount > 0) {
            return $limit ? $elements->searchHits[0]->valueObject : $elements->searchHits;
        }

        return false;
    }

    /**
     * @return array|\Twig_Function[]
     */
    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('getLiveFeed', array($this, 'getLiveFeed')),
            new \Twig_SimpleFunction('getSocialItem', array($this, 'getSocialItem'))
        );
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'SocialNetworkFunctions';
    }
}